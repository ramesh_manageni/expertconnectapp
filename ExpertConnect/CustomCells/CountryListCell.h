//
//  CountryListCell.h
//  ExpertConnectApp
//
//  Created by Nadeem on 21/10/16.
//  Copyright © 2016 user. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountryListCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *countryNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *countryCodeLabel;

@end
