//
// Created by hhtopcu.
// Copyright (c) 2016 hhtopcu. All rights reserved.
//

import Foundation

final class SubCategoryViewModel {
    var categoryId: String = ""
    
    init(categoryId: String) {
        self.categoryId = categoryId
    }
}
