//
// Created by hhtopcu.
// Copyright (c) 2016 hhtopcu. All rights reserved.
//

import Foundation

/** Use Object Mapper */
final class SubCategoryDomainModel {
    var categoryId: String = ""

    init(categoryId: String) {
        self.categoryId = categoryId
    }
}
