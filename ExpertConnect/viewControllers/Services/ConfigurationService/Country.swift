//
//  Country.swift
//  Mezuka
//
//  Created by Hasan H. Topcu on 22/09/2016.
//  Copyright © 2016 Mezuka. All rights reserved.
//

import Foundation

class Country {
    var name = ""
    var code = ""
    
    init(name: String, code: String) {
        self.name = name
        self.code = code
    }
}
