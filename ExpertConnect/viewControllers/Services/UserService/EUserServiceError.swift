//
//  EUserServiceError.swift
//  Mezuka
//
//  Created by Hasan H. Topcu on 27/09/2016.
//  Copyright © 2016 Mezuka. All rights reserved.
//

import Foundation

public enum EUserServiceError: Error {
    case UnknownError
}
