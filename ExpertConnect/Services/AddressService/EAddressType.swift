//
//  EAddressType.swift
//  Mezuka
//
//  Created by Hasan H. Topcu on 12/10/2016.
//  Copyright © 2016 Mezuka. All rights reserved.
//

import Foundation

enum EAddressType: Int {
    case stored = 0
    case recent = 1
}
