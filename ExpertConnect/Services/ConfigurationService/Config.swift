//
//  Config.swift
//  Mezuka
//
//  Created by Hasan H. Topcu on 22/09/2016.
//  Copyright © 2016 Mezuka. All rights reserved.
//

import Foundation

class Config {
    var googleMapKey: String = ""
    var countries: [String: Country] = [:]
}
