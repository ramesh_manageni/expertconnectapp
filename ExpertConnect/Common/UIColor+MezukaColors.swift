//
//  UIColor+MezukaColors.swift
//  Mezuka
//
//  Created by Hasan H. Topcu on 18/09/2016.
//  Copyright © 2016 Mezuka. All rights reserved.
//

import UIKit

extension UIColor {
    public class var mezukaRed: UIColor
    {
        return UIColor(red: 228/255, green: 38/255, blue: 39/255, alpha: 1.0)
    }
    
    public class var mezukaDark: UIColor
    {
        return UIColor(red: 55/255, green: 54/255, blue: 54/255, alpha: 1.0)
    }
    
    public class var mezukaMenuText: UIColor
    {
        return UIColor(colorLiteralRed: 201/255, green: 187/255, blue: 187/255, alpha: 1.0)
    }
    
    public class var mezukaViewStoreGreen: UIColor
    {
        return UIColor(colorLiteralRed: 45/255, green: 141/255, blue: 1/255, alpha: 1.0)
    }
    
    public class var mezukaActiveGreen: UIColor
    {
        return UIColor(colorLiteralRed: 77/255, green: 153/255, blue: 56/255, alpha: 1.0)
    }
    
    public class var mezukaPassiveGreen: UIColor
    {
        return UIColor(colorLiteralRed: 163/255, green: 207/255, blue: 150/255, alpha: 1.0)
    }
    
    public class var mezukaErrorRed: UIColor
    {
        return UIColor(red: 196/255, green: 31/255, blue: 32/255, alpha: 1.0)
    }
    
    public class var mezukaSuccessGreen: UIColor
    {
        return UIColor(red: 54/255, green: 102/255, blue: 41/255, alpha: 1.0)
    }
    
    public class var ExpertConnectRed: UIColor
    {
        return UIColor(red: 242/255, green: 90/255, blue: 11/255, alpha: 1.0)
    }

}
